def bubble_sort(arr, order='asc'):
    if not isinstance(arr, list):
        raise TypeError("arg must be list")
    n = len(arr)
    operations = 0
    for i in range(1, n):
        for j in range(n - 1):
            operations += 1
            if arr[j] > arr[j + 1]:
                arr[j], arr[j + 1] = arr[j + 1], arr[j]
    if order == 'asc':
        return arr, operations
    elif order == 'desc':
        return arr[::-1], operations
    else:
        raise TypeError("order have args asc and desc")


def insertion_sort(arr, order='asc'):
    if not isinstance(arr, list):
        raise TypeError("arg must be list")
    operations = 0
    for i in range(len(arr)):
        key = arr[i]
        j = i
        while j > 0 and key < arr[j-1]:
            arr[j] = arr[j-1]
            j -= 1
            operations += 1
        arr[j] = key
    if order == 'asc':
        return arr, operations
    elif order == 'desc':
        return arr[::-1], operations
    else:
        raise TypeError("order have args asc and desc")


def shell_sort(arr, order='asc'):
    if not isinstance(arr, list):
        raise TypeError("arg must be list")
    n = len(arr)
    gap = n // 2
    operations = 0
    while gap > 0:
        for i in range(gap, n):
            temp = arr[i]
            j = i
            while j >= gap and arr[j - gap] > temp:
                arr[j] = arr[j - gap]
                j -= gap
                operations += 1
            arr[j] = temp
        gap //= 2
    if order == 'asc':
        return arr, operations
    elif order == 'desc':
        return arr[::-1], operations
    else:
        raise TypeError("order have args asc and desc")


def quick_sort(arr, order='asc', operations=0):
    if not isinstance(arr, list):
        raise TypeError("arg must be list")
    if len(arr) <= 1:
        return arr, operations
    else:
        pivot = arr[0]
        less = [x for x in arr[1:] if x <= pivot]
        operations += len(arr[1:])
        greater = [x for x in arr[1:] if x > pivot]
        operations += len(arr[1:])
        if order == 'asc':
            less, operations = quick_sort(less, order, operations)
            greater, operations = quick_sort(greater, order, operations)
            return less + [pivot] + greater, operations
        elif order == 'desc':
            greater, operations = quick_sort(greater, order, operations)
            less, operations = quick_sort(less, order, operations)
            return greater + [pivot] + less, operations
        else:
            raise TypeError("order have args asc and desc")

import random
import timeit
import memory_profiler
from sorting_algorithms import bubble_sort, insertion_sort, shell_sort, quick_sort
import cpuinfo


def main():
    cache_size = int(cpuinfo.get_cpu_info()["l3_cache_size"] / (1024 * 1024))
    lists = [[random.randint(-1000, 1000) for _ in range(i)] for i in
             range(2 * cache_size, 20 * cache_size + cache_size, 2 * cache_size)]
    funcs = [bubble_sort, insertion_sort, shell_sort, quick_sort]
    for func in funcs:
        print(func.__name__)
        for arr in lists:
            arr = arr.copy()
            start = timeit.default_timer()
            _, operations = func(arr)
            finish = timeit.default_timer()
            print(f'{len(arr)}                         {finish - start} sec                         {operations} operations')
        print()
        print()

    for func in funcs:
        print(func.__name__)
        for arr in lists:
            arr = arr.copy()
            _, operations = func(arr)
            print(f'{len(arr)}                         {memory_profiler.memory_usage((func, (arr,)), max_usage=True)} MB                         {operations} operations')
        print()
        print()


if __name__ == '__main__':
    main()


import pytest
from sorting_algorithms import bubble_sort, insertion_sort, shell_sort, quick_sort

# Создаем тестовые данные
test_data = [
    ([4, 7, 1, 9, 3, 6, 2, 8, 5, 0], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]),
    ([9, 8, 7, 6, 5, 4, 3, 2, 1, 0], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]),
    ([3, 3, 1, 2, 2, 4], [1, 2, 2, 3, 3, 4]),
    ([], []),
    ([1], [1]),
]

# Тесты на пузырьковую сортировку
def test_bubble_sort_ascending():
    for data in test_data:
        sorted_arr, _ = bubble_sort(data[0].copy())
        assert sorted_arr == data[1]

def test_bubble_sort_descending():
    for data in test_data:
        sorted_arr, _ = bubble_sort(data[0].copy(), order='desc')
        assert sorted_arr == data[1][::-1]

# Тесты на сортировку вставками
def test_insertion_sort_ascending():
    for data in test_data:
        sorted_arr, _ = insertion_sort(data[0].copy())
        assert sorted_arr == data[1]

def test_insertion_sort_descending():
    for data in test_data:
        sorted_arr, _ = insertion_sort(data[0].copy(), order='desc')
        assert sorted_arr == data[1][::-1]

# Тесты на сортировку Шелла
def test_shell_sort_ascending():
    for data in test_data:
        sorted_arr, _ = shell_sort(data[0].copy())
        assert sorted_arr == data[1]

def test_shell_sort_descending():
    for data in test_data:
        sorted_arr, _ = shell_sort(data[0].copy(), order='desc')
        assert sorted_arr == data[1][::-1]

# Тесты на быструю сортировку
def test_quick_sort_ascending():
    for data in test_data:
        sorted_arr, _ = quick_sort(data[0].copy())
        assert sorted_arr == data[1]

def test_quick_sort_descending():
    for data in test_data:
        sorted_arr, _ = quick_sort(data[0].copy(), order='desc')
        assert sorted_arr == data[1][::-1]

# Тест на корректность входных данных
def test_input_validation():
    with pytest.raises(TypeError):
        bubble_sort('')
    with pytest.raises(TypeError):
        insertion_sort('')
    with pytest.raises(TypeError):
        shell_sort({})
    with pytest.raises(TypeError):
        quick_sort('')